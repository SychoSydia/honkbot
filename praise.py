#!/usr/bin/env python

__version__ = '0.2'
__releasedate__ = '2009-12-23'
__author__ = 'Ryan McGreal <ryan@quandyfactory.com>'
__homepage__ = 'http://quandyfactory.com/insult'
__repository__ = 'http://gist.github.com/258915'
__copyright__ = 'Copyright (C) 2009 by Ryan McGreal. Licenced under GPL version 2. http://www.gnu.org/licenses/gpl-2.0.html'

def generate_praise():
    """
    Generates an Elizabethan compliment.
    Praise terms are via: https://www.shakespeareoutloud.ca/elizabethan-compliments
    Based on Ryan McGreal's insult generator http://quandyfactory.com/insult
    """
    from random import randint
    words = (
        ('rare', 'sugared', 'precious', 'dutiful', 'damasked', 'flowering', 'gallant', 'celestial', 'sweet', 'saucy', 'sportful', 'artful', 'heavenly', 'yarely', 'tuneful', 'courteous', 'delicate', 'silken', 'brave', 'complete', 'vasty', 'pleasing', 'cheek-rosy', 'deserving', 'melting', 'wholesome', 'fruitful',),
        ('honey-tongued', 'well-wishing', 'berhyming', 'fair-faced', 'five-fingered-tied', 'heart-inflaming', 'not-answering', 'spleenative', 'softly-sprighted', 'smooth-faced', 'sweet-suggesting', 'swinge-buckling', 'tender-hearted', 'tender-feeling', 'thunder-darting', 'tiger-booted', 'lustyhooded', 'time-pleasing', 'superstitious', 'sympathizing', 'sweet-tongued', 'weeping-ripe', 'well-favoured', 'young-eyed', 'sweet-mouthing', 'best-tempered', 'well-graced',),
        ('nymph', 'ornament', 'toast', 'curiosity', 'apple-john', 'bilbo', 'cuckoo-bud', 'nose-herb', 'gamester', 'ouch', 'goddess', 'night-cap', 'delight', 'watercake', 'umpire', 'sprite', 'song', 'welsh cheese', 'kissing-comfit', 'wit-cracker', 'hawthorn-bud', 'valentine', 'smilet', 'true-penny', 'primrose', 'path', 'gaudy-night', 'pigeon-egg',),
        )
    praise_list = (
        words[0][randint(0,len(words[0])-1)],
        words[1][randint(0,len(words[1])-1)],
        words[2][randint(0,len(words[2])-1)],
        )
    vowels = 'AEIOU'
    article = 'an' if praise_list[0][0] in vowels else 'a'
    return 'Thou art %s %s, %s %s.' % (article, praise_list[0], praise_list[1], praise_list[2])
    
if __name__ == '__main__':
    print(generate_praise())